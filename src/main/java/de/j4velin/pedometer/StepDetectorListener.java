/*
 * Copyright 2013 Thomas Hoffmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.j4velin.pedometer;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.j4velin.pedometer.ui.Activity_Main;
import de.j4velin.pedometer.util.API23Wrapper;
import de.j4velin.pedometer.util.API26Wrapper;
import de.j4velin.pedometer.util.Logger;
import de.j4velin.pedometer.util.Util;
import de.j4velin.pedometer.widget.WidgetUpdateService;

/**
 * Background service which keeps the step-sensor listener alive to always get
 * the number of steps since boot.
 * <p/>
 * This service won't be needed any more if there is a way to read the
 * step-value without waiting for a sensor event
 */

/**
 * Similar to SensorListener that tracks batches of steps. This class listens for individual steps
 */
public class StepDetectorListener extends Service implements SensorEventListener {

    private static final int STEP_DURATION_CUTOFF = 5000; // in milliseconds
    private static final int STEP_LOG_SIZE = 50; // number of step entries to keep and compare (for mean and sd)

    public final static int NOTIFICATION_ID = 1;
    private final static long MICROSECONDS_IN_ONE_MINUTE = 60000000;
    private final static long SAVE_OFFSET_TIME = AlarmManager.INTERVAL_HOUR;
    private final static int SAVE_OFFSET_STEPS = 500;

    private static int steps;
    private static int lastSaveSteps;
    private static long lastSaveTime;

    private final BroadcastReceiver shutdownReceiver = new ShutdownRecevier();

    private SharedPreferences preferences;
    private static final String STEPS_LOG = "steps_log";
    private static final String LAST_STEP_TIME = "last_step_time";

    private String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
    private String fileName = "AnalysisData.csv";
    private String filePath = baseDir + File.separator + fileName;
    private File f = new File(filePath);
    private CSVWriter writer;

    @Override
    public void onAccuracyChanged(final Sensor sensor, int accuracy) {
        // nobody knows what happens here: step value might magically decrease
        // when this method is called...
        if (BuildConfig.DEBUG) Logger.log(sensor.getName() + " accuracy changed: " + accuracy);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (event.values[0] > Integer.MAX_VALUE) {
            if (BuildConfig.DEBUG) Logger.log("probably not a real value: " + event.values[0]);
            return;
        } else {
            /*
            steps = (int) event.values[0];
            updateIfNecessary();
             */
            long lastStepTime = preferences.getLong(LAST_STEP_TIME, 0);
            long eventTime = (System.currentTimeMillis() - SystemClock.elapsedRealtime()) * 1000000 + event.timestamp; // event time in nanoseconds
            long stepTime = eventTime / 1000000;
            int stepDuration = (int) (stepTime - lastStepTime);
            Logger.log("Step duration is " + stepDuration + " ms");

            // add step to last 50 tracker

            Gson gson = new Gson();
            String stepsJSON = preferences.getString(STEPS_LOG, "");
            ArrayList<Integer> stepDurations = new ArrayList<>();
            if(stepsJSON.isEmpty()){
                // no stored steps
            }else {
                Type type = new TypeToken<List<String>>() {
                }.getType();
                stepDurations = gson.fromJson(stepsJSON, type);
                if(stepDurations.size() > STEP_LOG_SIZE){
                    // remove first entry (in preperation for adding an entry later)
                    stepDurations.remove(0);
                }
            }
            stepDurations.add(stepDuration);
            boolean addStep = checkSteps();
            Logger.log("Add step? " + addStep);
            if(addStep){
                addStep();
            }

            // store to preferences
            // convert stepDurations into JSON
            String stepDurationsJSON = gson.toJson(stepDurations);
            Logger.log(stepDurationsJSON);
            Logger.log("Storing " + stepTime + " time into storage");
            SharedPreferences.Editor editor = preferences.edit();
            editor.putLong(LAST_STEP_TIME, stepTime);
            editor.putString(STEPS_LOG, stepDurationsJSON);
            editor.commit();





            /**
             * This section is only for debugging. Not for final release
             */
            /*
            Logger.log("Step detected");
            // go thru step checking algorithm
            int detectedSteps = preferences.getInt("DETECTED_STEPS", 0);
            Logger.log("Steps from storage are " + detectedSteps);
            detectedSteps++;


            // time step was taken
            //long eventTime = (System.currentTimeMillis() - SystemClock.elapsedRealtime()) * 1000000 + event.timestamp; // event time in nanoseconds
            //long stepTime = eventTime / 1000000;
            Logger.log("Step time: " + stepTime); // convert nanoseconds to milliseconds, its close enough for this
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date(stepTime);
            Logger.log("Step date: "  + date);

            // store to preferences
            Logger.log("Storing " + detectedSteps + " into storage");
            //SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("DETECTED_STEPS", detectedSteps);
            editor.commit();

            String[] text = {String.valueOf(detectedSteps), String.valueOf(eventTime / 1000000), String.valueOf(preferences.getInt("STEP_COUNT", 0))};
            // File exist
            try {
                if (f.exists() && !f.isDirectory()) {
                    FileWriter mFileWriter = new FileWriter(filePath, true);
                    writer = new CSVWriter(mFileWriter);
                } else {
                    writer = new CSVWriter(new FileWriter(filePath));
                }
                //String[] data = {"Ship Name", "Scientist Name", "...", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").formatter.format(date)});
                Logger.log("file path: " + filePath);
                writer.writeNext(text);

                writer.close();
            }catch(IOException e){
                e.printStackTrace();
            }

            /**
             * Debugging section above
             */

        }
    }

    private boolean checkSteps(){
        Logger.log("Checking last step");

        // get list of previous steps if available
        Gson gson = new Gson();
        String stepsJSON = preferences.getString(STEPS_LOG, "");
        if(stepsJSON.isEmpty()){
            // no stored steps
        }else {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            ArrayList<String> stepDurations = gson.fromJson(stepsJSON, type);

            // normalize list so that durations over 5s are excluded
            ArrayList<Integer> normalizedStepDurations = new ArrayList<>();
            for (String stepDurationString : stepDurations) {
                int stepDuration = Integer.parseInt(stepDurationString);
                if (stepDuration <= STEP_DURATION_CUTOFF) {
                    normalizedStepDurations.add(stepDuration);
                }
            }

            // get average and std of previous steps
            float mean = mean(normalizedStepDurations);
            float sd = standardDeviation(normalizedStepDurations);
            Logger.log("AVG: " + mean);
            Logger.log("STD: " + sd);

            // if it falls within the average step duration +/- 1 std then add the step to the step count
            if (stepDurations.size() > 0) {
                int lastStepDuration = Integer.parseInt(stepDurations.get(stepDurations.size() - 1));
                if (lastStepDuration > 0 && lastStepDuration > (mean - sd) && lastStepDuration < (mean + sd)){
                    return true;
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    private float standardDeviation(List<Integer> numbers){
        float mean = mean(numbers);
        double temp = 0;

        for(int number : numbers){
            double squareDeltaMean = Math.pow(number - mean, 2);
            temp += squareDeltaMean;
        }

        double meanOfDiffs = (double) temp / (double) (numbers.size());

        return (float) Math.sqrt(meanOfDiffs);
    }

    private float mean(List<Integer> numbers){
        float sum = 0;
        for(float number : numbers){
            sum += number;
        }
        return sum / numbers.size();
    }

    private void addStep(){
        Database db = Database.getInstance(this);
        if (db.getSteps(Util.getToday()) == Integer.MIN_VALUE) {
            //int pauseDifference = steps - getSharedPreferences("pedometer", Context.MODE_PRIVATE).getInt("pauseCount", steps);
            //db.insertNewDay(Util.getToday(), steps - pauseDifference);
            db.insertNewDay(Util.getToday(), 0);
            /*
            if (pauseDifference > 0) {
                // update pauseCount for the new day
                getSharedPreferences("pedometer", Context.MODE_PRIVATE).edit().putInt("pauseCount", steps).commit();
            }
             */
        }
        //db.saveCurrentSteps(db.getCurrentSteps() + 1);
        db.insertStep(Util.getToday(), 1);
        //db.saveCurrentSteps(1);
        startService(new Intent(this, WidgetUpdateService.class));
        db.getStepsOnDate(Util.getToday());
    }

    /**
     * @return true, if notification was updated
     */
    private boolean updateIfNecessary() {
        /*
        if (steps > lastSaveSteps + SAVE_OFFSET_STEPS ||
                (steps > 0 && System.currentTimeMillis() > lastSaveTime + SAVE_OFFSET_TIME)) {
            if (BuildConfig.DEBUG) Logger.log("saving steps: steps=" + steps + " lastSave=" + lastSaveSteps + " lastSaveTime=" + new Date(lastSaveTime));
            Database db = Database.getInstance(this);
            if (db.getSteps(Util.getToday()) == Integer.MIN_VALUE) {
                int pauseDifference = steps - getSharedPreferences("pedometer", Context.MODE_PRIVATE).getInt("pauseCount", steps);
                db.insertNewDay(Util.getToday(), steps - pauseDifference);
                if (pauseDifference > 0) {
                    // update pauseCount for the new day
                    getSharedPreferences("pedometer", Context.MODE_PRIVATE).edit().putInt("pauseCount", steps).commit();
                }
            }
            db.saveCurrentSteps(steps);
            db.close();
            lastSaveSteps = steps;
            lastSaveTime = System.currentTimeMillis();
            showNotification(); // update notification
            startService(new Intent(this, WidgetUpdateService.class));
            return true;
        } else {
            return false;
        }
         */
        return false;
    }

    private void showNotification() {
        if (Build.VERSION.SDK_INT >= 26) {
            startForeground(NOTIFICATION_ID, getNotification(this));
        } else if (getSharedPreferences("pedometer", Context.MODE_PRIVATE).getBoolean("notification", true)) {
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(NOTIFICATION_ID, getNotification(this));
        }
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        reRegisterSensor();
        registerBroadcastReceiver();
        if (!updateIfNecessary()) {
            showNotification();
        }

        // restart service every hour to save the current step count
        long nextUpdate = Math.min(Util.getTomorrow(), System.currentTimeMillis() + AlarmManager.INTERVAL_HOUR);
        if (BuildConfig.DEBUG) Logger.log("next update: " + new Date(nextUpdate).toLocaleString());
        AlarmManager am = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getService(getApplicationContext(), 2, new Intent(this, StepDetectorListener.class),
                        PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= 23) {
            API23Wrapper.setAlarmWhileIdle(am, AlarmManager.RTC, nextUpdate, pi);
        } else {
            am.set(AlarmManager.RTC, nextUpdate, pi);
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) Logger.log("SensorListener onCreate");
    }

    @Override
    public void onTaskRemoved(final Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (BuildConfig.DEBUG) Logger.log("sensor service task removed");
        // Restart service in 500 ms
        ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC, System.currentTimeMillis() + 500, PendingIntent
                        .getService(this, 3, new Intent(this, StepDetectorListener.class), 0));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (BuildConfig.DEBUG) Logger.log("SensorListener onDestroy");
        try {
            SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
            sm.unregisterListener(this);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Logger.log(e);
            e.printStackTrace();
        }
    }

    public static Notification getNotification(final Context context) {
        if (BuildConfig.DEBUG) Logger.log("getNotification");
        SharedPreferences prefs = context.getSharedPreferences("pedometer", Context.MODE_PRIVATE);
        int goal = prefs.getInt("goal", 10000);
        Database db = Database.getInstance(context);
        //int today_offset = db.getSteps(Util.getToday());
        if (steps == 0)
            //steps = db.getCurrentSteps(); // use saved value if we haven't anything better
            steps = db.getStepsOnDate(Util.getToday());
        db.close();
        Notification.Builder notificationBuilder = Build.VERSION.SDK_INT >= 26 ? API26Wrapper.getNotificationBuilder(context) : new Notification.Builder(context);
        if (steps > 0) {
            //if (today_offset == Integer.MIN_VALUE) today_offset = -steps;
            notificationBuilder.setProgress(goal, steps, false)
                    .setContentText(steps >= goal ?
                            context.getString(R.string.goal_reached_notification,
                                    NumberFormat.getInstance(Locale.getDefault())
                                            .format((steps))) :
                            context.getString(R.string.notification_text,
                                    NumberFormat.getInstance(Locale.getDefault())
                                            .format((goal - steps))));
        } else { // still no step value?
            notificationBuilder.setContentText(
                    context.getString(R.string.your_progress_will_be_shown_here_soon));
        }
        notificationBuilder.setPriority(Notification.PRIORITY_MIN).setShowWhen(false)
                .setContentTitle(context.getString(R.string.notification_title)).setContentIntent(
                PendingIntent.getActivity(context, 0, new Intent(context, Activity_Main.class),
                        PendingIntent.FLAG_UPDATE_CURRENT)).setSmallIcon(R.drawable.ic_notification)
                .setOngoing(true);
        return notificationBuilder.build();
    }

    private void registerBroadcastReceiver() {
        if (BuildConfig.DEBUG) Logger.log("register broadcastreceiver");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        registerReceiver(shutdownReceiver, filter);
    }

    private void reRegisterSensor() {
        if (BuildConfig.DEBUG) Logger.log("re-register sensor listener");
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        try {
            sm.unregisterListener(this);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Logger.log(e);
            e.printStackTrace();
        }

        if (BuildConfig.DEBUG) { 
            Logger.log("step detector: " + sm.getSensorList(Sensor.TYPE_STEP_DETECTOR).size());
            if (sm.getSensorList(Sensor.TYPE_STEP_COUNTER).size() < 1) return; // emulator
            Logger.log("detect: " + sm.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR).getName());
        }

        // enable batching with delay of max 5 min
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),
                SensorManager.SENSOR_DELAY_NORMAL, (int) (5 * MICROSECONDS_IN_ONE_MINUTE));

        // initiate shared preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }
}
